# ab

> ab architecture

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# serve with external host 
$ npm run dev-external

# API. Run in the second console
$ npm run api

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

``` bash
# Просмотр задач демона:
$ pm2 list

# Остановка задач:
$ pm2 stop api
$ pm2 stop server

# Затем выполняем обновление файлов проекта

# Запуск задач
$ pm2 start api
$ pm2 start server
```
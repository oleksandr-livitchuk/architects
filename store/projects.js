export const state = () => ({
  projects: []
});

export const mutations = {
  setProjects(state, newValue) {
    state.projects = newValue;
  }
};

export const getters = {
  getProjectsByCategory: state => category => {
    return state.projects.filter(project => {
      return project.category === category;
    });
  },
  getProjectById: state => id => {
    return state.projects.filter(project => {
      return +project.slug === id;
    });
  }
};

export const state = () => ({
  loading: true,
  menuVisible: false,
  socialsBoxVisible: false,
  page: "index",
  footer: null,
  menuData: null,
  activeLang: "ru",
  isFormModalOpened: false,
  requestFormText: null,
  panelsWhite: false,
  scrollDepth: 0,
  listScrollAmount: null,
  projectPrev: null
});

export const mutations = {
  setMenuVisible(state, newValue) {
    state.menuVisible = newValue;
  },
  setSocialsBoxVisible(state, newValue) {
    state.socialsBoxVisible = newValue;
  },
  setLoading(state, newValue) {
    state.loading = newValue;
  },
  updatePage(state, pageName) {
    state.page = pageName;
  },
  setFooter(state, newValue) {
    state.footer = newValue;
  },
  setMenuData(state, newValue) {
    state.menuData = newValue;
  },
  setSocialsBlock(state, newValue) {
    state.socials_block = newValue;
  },
  setPreloaderText(state, newValue) {
    state.preloader_text = newValue;
  },
  setAudioLink(state, newValue) {
    state.audio_link = newValue;
  },
  setHomeImagesLinks(state, newValue) {
    state.home_images = newValue;
  },
  setActiveLang(state, newValue) {
    state.activeLang = newValue;
  },
  setProjectBg(state, newValue) {
    state.projectBg = newValue;
  },
  setFormModalClosed(state, newValue) {
    state.isFormModalOpened = newValue;
  },
  setFormModalOpened(state, newValue) {
    state.isFormModalOpened = newValue;
  },
  setFormText(state, newValue) {
    state.requestFormText = newValue;
  },
  setPanelsColor(state, newValue) {
    state.panelsWhite = newValue;
  },
  setScrollDepth(state, newValue) {
    state.scrollDepth = newValue;
  },
  setLivingFilter(state, newValue) {
    state.livingFilter = newValue;
  },
  setPublicFilter(state, newValue) {
    state.publicFilter = newValue;
  },
  setProjectPrev(state, newValue) {
    state.projectPrev = newValue;
  },
  setListScrollAmount(state, newValue) {
    state.listScrollAmount = newValue;
  }
};

export const actions = {
  setListScrollAmount({ commit }, value) {
    commit("setListScrollAmount", value);
  },
  setProjectPrev({ commit }, value) {
    commit("setProjectPrev", value);
  },
  setScrollDepth({ commit }, value) {
    commit("setScrollDepth", value);
  },
  setPanelsWhite({ commit }, value) {
    commit("setPanelsColor", value);
  },
  changeProjectBg({ commit }, color) {
    const newColor = color ? color : "#AC9898";

    commit("setProjectBg", newColor);
  },
  nuxtServerInit({ commit }, ctx) {
    /*return loadData(
      "http://architects.nextpage.com.ua/api/projects",
      commit,
      $axios
    );*/

    const lang = ctx.app.$cookies.get("lang");

    if (!lang) {
      ctx.app.$cookies.set("lang", "ru");
    }

    commit("setActiveLang", lang);

    const endpoint =
      lang === "en"
        ? "http://admin-en.ab-architects.ru/api/projects"
        : "http://admin.ab-architects.ru/api/projects";

    return loadData(endpoint, commit, ctx.$axios);
  },
  showMenu({ commit }) {
    commit("setMenuVisible", true);
  },
  hideMenu({ commit }) {
    commit("setMenuVisible", false);
  },
  showSocialsBox({ commit }) {
    commit("setSocialsBoxVisible", true);
  },
  hideSocialsBox({ commit }) {
    commit("setSocialsBoxVisible", false);
  },
  finishLoading({ commit }) {
    commit("setLoading", false);
  },
  changeLang(store, { ctx, lang }) {
    ctx.$cookies.set("lang", lang);
  },
  /*changeLang(ctx, lang) {
    if (lang === "ru") {
      ctx.commit("setActiveLang", "ru");
      return loadData(
        "http://architects.nextpage.com.ua/api/projects",
        ctx.commit,
        this.$axios
      );
    } else if (lang === "en") {
      ctx.commit("setActiveLang", "en");
      return loadData(
        "http://architects-en.nextpage.com.ua/api/projects",
        ctx.commit,
        this.$axios
      );
    }
  },*/
  openFormModal({ commit }) {
    commit("setFormModalOpened", true);
  },
  closeFormModal({ commit }) {
    commit("setFormModalClosed", false);
  },
  setData({ commit }, response) {
    commit("projects/setProjects", response.projects);
    commit("offices/setOffices", response.offices);
    commit("digest/setDigest", response.digest);
    commit("digest/setDigestHeader", response.digest_header);
    commit("about/setAbout", response.about);
    commit("services/setServices", response.services);
    commit("contacts/setContacts", response.contacts);
    commit("setSocialsBlock", response.socials_block);
    commit("setPreloaderText", response.preloader_text);
    commit("setAudioLink", response.audio_link);
    commit("setFooter", response.footer);
    commit("setMenuData", response.menu);
    commit("setHomeImagesLinks", response.home_images);
    commit("setFormText", response.requestForm);
    commit("setLivingFilter", response.living_filters);
    commit("setPublicFilter", response.public_filters);
  }
};

export const getters = {
  menuVisible: state => {
    return state.menuVisible;
  },
  socialsBoxVisible: state => {
    return state.socialsBoxVisible;
  },
  loading: state => {
    return state.loading;
  },
  isFormModalOpened: state => state.isFormModalOpened
  //getActivelang: state => state.activelang
};

function loadData(link, commit, axios) {
  return (
    axios
      .$get(link, {
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
          "Access-Control-Allow-Headers":
            "Origin, Accept, Content-Type, Authorization, Access-Control-Allow-Origin"
        },
        responseType: "json"
      })
      // return $axios.$get('http://architects.nextpage.com.ua/api/projects')
      // return $axios.$get('http://localhost:3004/api')
      .then(response => {
        commit("projects/setProjects", response.projects);
        commit("offices/setOffices", response.offices);
        commit("digest/setDigest", response.digest);
        commit("digest/setDigestHeader", response.digest_header);
        commit("about/setAbout", response.about);
        commit("services/setServices", response.services);
        commit("contacts/setContacts", response.contacts);
        commit("setSocialsBlock", response.socials_block);
        commit("setPreloaderText", response.preloader_text);
        commit("setAudioLink", response.audio_link);
        commit("setFooter", response.footer);
        commit("setMenuData", response.menu);
        commit("setHomeImagesLinks", response.home_images);
        commit("setFormText", response.requestForm);
        commit("setLivingFilter", response.living_filters);
        commit("setPublicFilter", response.public_filters);
      })
  );
}

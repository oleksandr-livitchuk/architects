export const state = () => ({});

export const mutations = {
	setDigest(state, newValue) {
		state.digest = newValue;
	},
	setDigestHeader(state, newValue) {
		state.header = newValue;
	}
};

export const getters = {
	getDigest: state => category => {
		return state.digest
	},
	getDigestHeader: state => id => {
		return state.header
	}
};

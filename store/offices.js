export const state = () => ({
  offices: [],
	activeOfficeId: null
});

export const mutations = {
  setOffices(state, newValue) {
    state.offices = newValue;
  },
  setActiveOfficeId(state, newValue) {
    state.activeOfficeId = newValue;
  }
};

export const actions = {
  changeActiveOfficeId({commit}, newValue) {
		commit('setActiveOfficeId', newValue)
  }
};

export const getters = {
  getOffices: state => {
    return state.offices;
  },
  getActiveOfficeId: state => {
    return state.activeOfficeId;
  }
};

import Vue from 'vue'
import VueMq from 'vue-mq'

Vue.use(VueMq, {
	breakpoints: { // default breakpoints - customize this
		xs: 450,
		sm: 768,
		md: 1250,
		xlg:1920,
	},
	defaultBreakpoint: 'md' // customize this for SSR
});

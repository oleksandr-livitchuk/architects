import {SplitText} from '~/assets/js/SplitText';

export default ({app}, inject) => {
	inject('SplitText', SplitText);
}

import axios from "@nuxtjs/axios"
import store from "../store/index"

export function loadData(link) {
  return (
    axios
      .$get(link, {
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
          "Access-Control-Allow-Headers":
            "Origin, Accept, Content-Type, Authorization, Access-Control-Allow-Origin"
        },
        responseType: "json"
      })
      // return $axios.$get('http://architects.nextpage.com.ua/api/projects')
      // return $axios.$get('http://localhost:3004/api')
      .then((response) => {
        store.dispatch("setData", )
      })
  );
}
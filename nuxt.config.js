module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: "Architecture bureau",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width,user-scalable=no" },
      {
        hid: "description",
        name: "description",
        content: "Ab - Architecture bureau"
      },
      { name: "it-rating", content: "it-rat-5f706892c714d42206dbf7d335c40620" },
      { name: "cmsmagazine", content: "c30d024a3834392dc315e7a664baec5d" }
    ],
    link: [{ rel: "icon", type: "image/png", href: "/favicon.png" }]
    /*script: [
			{ src: '/MorphSVGPlugin.min.js' }
		]*/
  },
  /*
   ** Customize the progress bar color
   */
  cache: true,
  loading: { color: "#FFF", height: "0px" },
  /*
   ** Global styles
   */
  css: [
    //'plyr/dist/plyr.css',
    "~/assets/scss/index.scss"
  ],
  // layoutTransition: {
  //   name: 'layout',
  //   mode: 'out-in'
  // },
  /*
   ** Modules
   */
  buildModules: [
    [
      "@nuxtjs/google-analytics",
      {
        id: "UA-170999611-1"
      }
    ]
  ],
  modules: [
    "cookie-universal-nuxt",
    "@nuxtjs/style-resources",
    "@nuxtjs/svg",
    "@nuxtjs/axios",
    "@nuxtjs/svg-sprite",
    [
      "@nuxtjs/yandex-metrika",
      {
        id: "64921561",
        webvisor: true,
        clickmap: true,
        // useCDN:false,
        trackLinks: true,
        accurateTrackBounce: true
      }
    ],
    [
      "nuxt-facebook-pixel-module",
      {
        /* module options */
        track: "PageView",
        pixelId: "175987623333442",
        disabled: false
      }
    ]
  ],
  /*
   ** Global variables for components
   */
  styleResources: {
    scss: ["~/assets/scss/variables/_variables.index.scss"]
  },
  /*
   ** Router
   */
  router: {
    middleware: "pages"
  },
  /*
   ** Plugins
   */
  plugins: [
    //{src: '~/plugins/eventBus'},
    { src: "~/plugins/directives", mode: "client" },
    { src: "~/plugins/gsap", mode: "client" },
    { src: "~/plugins/vuebar", mode: "client" },
    //{src: '~/plugins/scrollTo', mode: 'client'},
    { src: "~/plugins/hover", mode: "client" },
    { src: "~/plugins/splitText", mode: "client" },
    { src: "~plugins/isotope.js", mode: "client" },
    { src: "~plugins/checkView.js", mode: "client" },
    { src: "~plugins/embed.js", mode: "client" },
    { src: "~plugins/mediaQuery.js" }
  ],
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        });
      }
    },
    //analyze: true,
    cache: true
  },
  vue: {
    config: {
      performance: true // you probably should detect dev mode here
    }
  }
  /*render: {
		bundleRenderer: {
			shouldPrefetch: (file, type) => {
				return false
			}
		}
	}*/
};
